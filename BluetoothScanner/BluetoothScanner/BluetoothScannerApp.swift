//
//  BluetoothScannerApp.swift
//  BluetoothScanner
//
//  Created by Philip Laskowski on 25.03.21.
//

import Common
import Model
import ViewModel
import View
import Networking

import SwiftUI

@main
struct BluetoothScannerApp: App {
    
    /* * * * * * * * * * * * * * * *
     * PLEASE READ README.md FIRST *
     * * * * * * * * * * * * * * * */
    
    var body: some Scene {
        
        WindowGroup {
            ZStack {
                TabView {
                    
                    // #1 Scanner that simulates simple numbered devices
                    NavigationView {
                        ViewFactory<DeviceScannerView>.make(
                            with: DeviceScannerView.ViewModel.Dependencies(
                                deviceScanner: MockNumberedDeviceScanner()
                            )
                        )
                        .navigationTitle("Mock Devices")
                    }
                    .navigationViewStyle(StackNavigationViewStyle())
                    .tabItem {
                        Label("Mock Devices", systemImage: "m.circle")
                    }
                    
                    // #2 Scanner that simulates real Bluetooth devices with the CoreBluetoothMock framework
                    NavigationView {
                        ViewFactory<DeviceScannerView>.make(
                            with: DeviceScannerView.ViewModel.Dependencies(
                                deviceScanner: MockBluetoothDeviceScanner()
                            )
                        )
                        .navigationTitle("Mock Bluetooth Devices")
                    }
                    .navigationViewStyle(StackNavigationViewStyle())
                    .tabItem {
                        Label("Mock Bluetooth Devices", systemImage: "b.circle")
                    }
                    
                    // #3 Scanner that scans for real Bluetooth devices with the CoreBluetooth framework
                    NavigationView {
                        ViewFactory<DeviceScannerView>.make(
                            with: DeviceScannerView.ViewModel.Dependencies(
                                deviceScanner: BluetoothDeviceScanner()
                            )
                        )
                        .navigationTitle("Bluetooth Devices")
                    }
                    .navigationViewStyle(StackNavigationViewStyle())
                    .tabItem {
                        Label("Bluetooth Devices", systemImage: "dot.radiowaves.left.and.right")
                    }
                }
            }
        }
    }
}

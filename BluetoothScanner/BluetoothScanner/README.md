# Bluetooth Scanner Demo
## Demo Project for New Motion

This demo project focuses on showcasing architectural and design patterns and coding style.

**Principles used**:        SOLID, Composite Reuse
**Patterns used**:          MVVM, Dependency Injection (Initializer), Factory, Composition, Publisher/Observer
**Frameworks used**:    SwiftUI, Combine, Foundation, CoreBluetooth, CoreBluetoothMock.

For better readability and clarity no tests have been added to the project.
In a real world project tests would be added and - depending on how the team
operates - written first following the _Red-Green-Refactor_ cycle of TDD.


## Features
For demo purposes, the app shows three separate tabs, each with a scanner for
nearby devices:

1. Tab #1 works with a scanner that simulates simple numbered devices

2. Tab #2 works with a scanner that simulates real Bluetooth devices with the CoreBluetoothMock framework

3. Tab #3 works with a scanner that scans for real Bluetooth devices with the CoreBluetooth framework

Scanned devices can be connected to and disconnected from.


## Control Flow

Each module has been isolated into a separate layer (Framework Target) and dependencies are resolved by linking
the targets and imports. There is no control or data flow between modules that are not linked.

This means for example that the View layer does not know anything about the Model layer, like it is supposed to
be in MVVM.

The app works with strict Initializer Dependency Injection which checks all correct dependencies already at
compile team (unlike a Service Locator, for example).

All views are written in SwiftUI and can be created using the `ViewFactory`. The `ViewFactory` makes use of
the strict use of Initializer Dependency Injection and Composition, which allows for a very centralized
composition of scenes and views with very loose coupling.

The basic flow of control is from outer layers to inner layers according to Clean Architecture.
The creation of views in the outermost layer happens in a simple three-step process using
strict Initializer Dependency Injection.

1. The dependencies for the ViewModel are created, for example with a Model and a Bluetooth Scanner (Networking Service).
2. The ViewModel is created with the dependencies.
2. The View is created with the ViewModel.

Code Example:

```
let dependencies    = View.ViewModel.Dependencies(Model, Networking Service, etc...)
let viewModel       = View.ViewModel(dependencies: dependencies)
let view            = View(viewModel: viewModel)
```


## Architecture / Modules

The project follows the MVVM pattern. Besides **Model**, **ViewModel** and **View**
two other layer **Networking** and **Common** have been added.

### Common
**Purpose**: Contains non-specific protocols and extensions that can be used by any module (except for _Model_)
**Dependencies**: no dependencies

### Model
**Purpose**: Contains model types only
**Dependencies**: no dependencies

### Networking
**Purpose**: Contains model types only
**Dependencies**: Common, Model, CoreBluetooth, CoreBluetoothMock

### ViewModel
**Purpose**: Contains model types only
**Dependencies**: Common, Networking, Model

### View
**Purpose**: Contains model types only
**Dependencies**: Common, Networking, ViewModel

Control Flow:
![Control Flow](ControlFlow.png)


## Next Steps

Next steps could be breaking down the architecture further in accordance wirh Clean Architecture by introducing
Domain, Presentation and Data layers. Moreover, tests should be added thoroughly.

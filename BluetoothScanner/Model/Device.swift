//
//  Device.swift
//  BluetoothScanner
//
//  Created by Philip Laskowski on 26.03.21.
//

public struct Device: Identifiable {
    
    public enum ConnectionStatus {
        case disconnected
        case connecting
        case connected
        case disconnecting
    }
    
    public let id: String
    public let name: String?
    public let connectionStatus: ConnectionStatus
    
    public init(id: String, name: String?, connectionStatus: ConnectionStatus = .disconnected) {
        self.id = id
        self.name = name
        self.connectionStatus = connectionStatus
    }
}

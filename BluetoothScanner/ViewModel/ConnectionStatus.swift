//
//  ConnectionStatus.swift
//  ViewModel
//
//  Created by Philip Laskowski on 28.03.21.
//

import Model

public enum ConnectionStatus {
    case disconnected
    case connecting
    case connected
    case disconnecting
}

extension ConnectionStatus: ModelCodable {
    
    public init(model: Device.ConnectionStatus) {
        switch model {
        case .connected:
            self = ConnectionStatus.connected
        case .connecting:
            self = ConnectionStatus.connecting
        case .disconnected:
            self = ConnectionStatus.disconnected
        case .disconnecting:
            self = ConnectionStatus.disconnecting
        }
    }
    
    public func toModel() -> Device.ConnectionStatus {
        switch self {
        case .connected:
            return Device.ConnectionStatus.connected
        case .connecting:
            return Device.ConnectionStatus.connecting
        case .disconnected:
            return Device.ConnectionStatus.disconnected
        case .disconnecting:
            return Device.ConnectionStatus.disconnecting
        }
    }
}

//
//  DeviceScannerViewModel.swift
//  BluetoothScanner
//
//  Created by Philip Laskowski on 25.03.21.
//

import Common
import Model
import Networking

import Foundation
import Combine

private typealias Protocols
    = DependenciesBased
    & DeviceConnecting
    & DeviceConnectionObserving
    & DeviceScanning

final public class DeviceScannerViewModel: ObservableObject, Protocols {
    
    public typealias DeviceViewModelType = DeviceViewModel
    
    public class Dependencies: HasDeviceScanner,
                               HasCancellable {
        public let deviceScanner: DeviceScanner
        public let cancellable = CancellableBag()
        public init(deviceScanner: DeviceScanner) {
            self.deviceScanner = deviceScanner
        }
    }
    
    @Published public private(set) var devices: [DeviceViewModelType] = []
    @Published public private(set) var isScanning: Bool = false
    
    public let dependencies: Dependencies
    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
        subscribeToDeviceScanning()
        subscribeToDeviceConnectionObserving()
    }
    
    deinit {
        stopScan()
        unsubscribeFromDeviceScanning()
        unsubscribeFromDeviceConnectionObserving()
    }
}

// MARK - Extensions

extension DeviceScannerViewModel {
    public func onScanningStatusChanged(_ isScanning: Bool) {
        self.isScanning = isScanning
    }
}

extension DeviceScannerViewModel {
    public func onDeviceScanned(_ device: Device) {
        guard devices.firstIndex(where: { $0.id == device.id }) == nil else {
            return
        }
        let deviceViewModel = DeviceViewModelType(model: device)
        devices.append(deviceViewModel)
    }
}

extension DeviceScannerViewModel {
    public func onDeviceConnectionStatusChanged(_ device: Device) {
        guard let index = self.devices.firstIndex(where: { $0.id == device.id }) else {
            return
        }
        let deviceViewModel = DeviceViewModelType(model: device)
        devices[index] = deviceViewModel
    }
}

//
//  DeviceConnecting.swift
//  ViewModel
//
//  Created by Philip Laskowski on 28.03.21.
//

import Common
import Model

public protocol DeviceConnecting {
    associatedtype DeviceViewModelType
    func connect(to device: DeviceViewModelType)
    func disconnect(from device: DeviceViewModelType)
}

public extension DeviceConnecting where Self: DependenciesBased,
                                        Dependencies: HasDeviceScanner,
                                        DeviceViewModelType: ModelEncodable,
                                        DeviceViewModelType.Model == Device {
    
    func connect(to device: DeviceViewModelType) {
        dependencies.deviceScanner.connect(to: device.toModel())
    }
    
    func disconnect(from device: DeviceViewModelType) {
        dependencies.deviceScanner.disconnect(from: device.toModel())
    }
}

public extension DeviceConnecting where Self: DependenciesBased,
                                        Dependencies: HasDeviceScanner,
                                        DeviceViewModelType: ModelEncodable,
                                        DeviceViewModelType == Self,
                                        DeviceViewModelType.Model == Device {
    
    func connect() {
        dependencies.deviceScanner.connect(to: self.toModel())
    }
    
    func disconnect() {
        dependencies.deviceScanner.disconnect(from: self.toModel())
    }
}

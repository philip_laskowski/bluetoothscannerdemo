//
//  DeviceConnectionObserving.swift
//  ViewModel
//
//  Created by Philip Laskowski on 28.03.21.
//

import Common
import Model

import Foundation

public protocol DeviceConnectionObserving {
    func onDeviceConnectionStatusChanged(_ device: Device)
}

public extension DeviceConnectionObserving where Self: AnyObject,
                                                 Self: DependenciesBased,
                                                 Dependencies: HasCancellable,
                                                 Dependencies: HasDeviceScanner {
    
    func subscribeToDeviceConnectionObserving() {
        
        dependencies
            .deviceScanner
            .deviceConnectionStatusChangedPublisher
            .receive(on: RunLoop.main)
            .sink { [weak self] device in
                guard let self = self else {
                    return
                }
                self.onDeviceConnectionStatusChanged(device)
            }
            .store(in: dependencies)
    }
    
    func unsubscribeFromDeviceConnectionObserving() {
        dependencies.unsubscribe()
    }
}

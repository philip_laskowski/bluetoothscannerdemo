//
//  ModelConvertible.swift
//  ViewModel
//
//  Created by Philip Laskowski on 28.03.21.
//

public protocol ModelDecodable {
    associatedtype Model
    init(model: Model)
}

public protocol ModelEncodable {
    associatedtype Model
    func toModel() -> Model
}

public protocol ModelCodable: ModelDecodable & ModelEncodable { }

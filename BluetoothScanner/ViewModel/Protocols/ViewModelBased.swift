//
//  ViewModelBased.swift
//  ViewModel
//
//  Created by Philip Laskowski on 27.03.21.
//

public protocol ViewModelBased {
    associatedtype ViewModel
    var viewModel: ViewModel { get }
    init(viewModel: ViewModel)
}

//
//  DeviceScanning.swift
//  ViewModel
//
//  Created by Philip Laskowski on 28.03.21.
//

import Common
import Model

import Combine
import Foundation

public protocol DeviceScanning {
    func startScan()
    func stopScan()
    func onScanningStatusChanged(_ isScanning: Bool)
    func onDeviceScanned(_ device: Device)
}

public extension DeviceScanning where Self: DependenciesBased,
                                      Dependencies: HasDeviceScanner {
    
    func startScan() {
        dependencies.deviceScanner.startScan()
    }
    
    func stopScan() {
        dependencies.deviceScanner.stopScan()
    }
}

public extension DeviceScanning where Self: AnyObject,
                                      Self: DependenciesBased,
                                      Dependencies: HasCancellable,
                                      Dependencies: HasDeviceScanner {
    
    func subscribeToDeviceScanning() {
        
        dependencies
            .deviceScanner
            .isScanningPublisher
            .receive(on: RunLoop.main)
            .sink { [weak self] isScanning in
                guard let self = self else {
                    return
                }
                self.onScanningStatusChanged(isScanning)
            }
            .store(in: dependencies)
        
        dependencies
            .deviceScanner
            .deviceScannedPublisher
            .receive(on: RunLoop.main)
            .sink { [weak self] device in
                guard let self = self else {
                    return
                }
                self.onDeviceScanned(device)
            }
            .store(in: dependencies)
    }
    
    func unsubscribeFromDeviceScanning() {
        dependencies.unsubscribe()
    }
}

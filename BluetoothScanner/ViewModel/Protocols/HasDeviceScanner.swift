//
//  HasDeviceScanner.swift
//  ViewModel
//
//  Created by Philip Laskowski on 28.03.21.
//

import Networking

public protocol HasDeviceScanner {
    var deviceScanner: DeviceScanner { get }
}

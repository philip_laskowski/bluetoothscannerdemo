//
//  DeviceViewModel.swift
//  BluetoothScanner
//
//  Created by Philip Laskowski on 26.03.21.
//

import Common
import Model

private typealias Protocols
    = Identifiable
    & DependenciesBased
    & ModelCodable

public struct DeviceViewModel: Protocols {
    
    public struct Dependencies {
        public let id: String
        public let name: String
        public let connectionStatus: ConnectionStatus
        
        public init(id: String, name: String, connectionStatus: ConnectionStatus) {
            self.id = id
            self.name = name
            self.connectionStatus = connectionStatus
        }
    }
    
    public var id: String {
        return dependencies.id
    }
    
    public let dependencies: Dependencies
    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension DeviceViewModel {
    
    public init(model: Device) {
        self.dependencies = Dependencies(
            id: model.id,
            name: model.name ?? "Connect to resolve name",
            connectionStatus: ConnectionStatus(
                model: model.connectionStatus
            )
        )
    }
    
    public func toModel() -> Device {
        Device(
            id: dependencies.id,
            name: dependencies.name,
            connectionStatus: dependencies.connectionStatus.toModel()
        )
    }
}

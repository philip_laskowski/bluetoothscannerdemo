//
//  DeviceDetailViewModel.swift
//  ViewModel
//
//  Created by Philip Laskowski on 28.03.21.
//

import Common
import Model
import Networking

import Combine

private typealias Protocols
    = Identifiable
    & DependenciesBased
    & DeviceConnecting
    & DeviceConnectionObserving
    & ModelEncodable

final public class DeviceDetailViewModel: ObservableObject, Protocols {
    
    public typealias DeviceViewModelType = DeviceDetailViewModel
    
    public class Dependencies: HasDeviceScanner,
                               HasCancellable {
        public let id: String
        public let name: String
        public let connectionStatus: ConnectionStatus
        public let deviceScanner: DeviceScanner
        public let cancellable = CancellableBag()
        
        public init(id: String, name: String, connectionStatus: ConnectionStatus, deviceScanner: DeviceScanner) {
            self.id = id
            self.name = name
            self.connectionStatus = connectionStatus
            self.deviceScanner = deviceScanner
        }
    }
    
    @Published public private(set) var connectionStatus: ConnectionStatus
    
    public var id: String {
        return dependencies.id
    }
    
    public let dependencies: Dependencies
    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
        self.connectionStatus = dependencies.connectionStatus
        subscribeToDeviceConnectionObserving()
    }
    
    deinit {
        unsubscribeFromDeviceConnectionObserving()
    }
}

// MARK - Extensions

extension DeviceDetailViewModel {
    public func toModel() -> Device {
        Device(
            id: dependencies.id,
            name: dependencies.name,
            connectionStatus: connectionStatus.toModel()
        )
    }
}

extension DeviceDetailViewModel {
    public func onDeviceConnectionStatusChanged(_ device: Device) {
        self.connectionStatus = ConnectionStatus(model: device.connectionStatus)
    }
}

////
////  BluetoothDeviceScanner.swift
////  BluetoothScanner
////
////  Created by Philip Laskowski on 25.03.21.
////

import Common
import Model

import Combine
import CoreBluetooth

private struct BluetoothDevice {
    let device: Device
    let peripheral: CBPeripheral
}

final public class BluetoothDeviceScanner: NSObject, DeviceScanner, HasCancellable {
    
    public let isScanningPublisher = PassthroughSubject<Bool, Never>()
    public let deviceScannedPublisher = PassthroughSubject<Device, Never>()
    public let deviceConnectionStatusChangedPublisher = PassthroughSubject<Device, Never>()
    public let cancellable = CancellableBag()
    
    private struct Constants {
        struct Services {
            struct Characteristics {
                static let writeable    = CBUUID(string: "00ff0000-1000-1000-1000-000000000001")
                static let readable     = CBUUID(string: "00ff0000-1000-1000-1000-000000000002")
                static let notify       = CBUUID(string: "00ff0000-1000-1000-1000-000000000003")
            }
        }
        static let queueLabel = "com.philiplaskowski.queue.bluetooth-scanning"
    }
    
    private let services: [CBUUID]
    private let queue: DispatchQueue
    private let centralManager: CBCentralManager
    
    private var scannedDevices: [Device.ID: BluetoothDevice] = [:]
    private var connectedDevices: [Device.ID: BluetoothDevice] = [:]
    
    init(services: [CBUUID]) {
        self.services = services
        self.queue = DispatchQueue(label: BluetoothDeviceScanner.Constants.queueLabel,
                                   qos: .background,
                                   attributes: .concurrent,
                                   autoreleaseFrequency: .workItem,
                                   target: nil)
        
        self.centralManager = CBCentralManager(delegate: nil, queue: queue)
        
        super.init()
        
        self.centralManager.delegate = self
        
        setupSubscriptions()
    }
    
    public convenience override init() {
        self.init(services: [
            Constants.Services.Characteristics.writeable,
            Constants.Services.Characteristics.readable,
            Constants.Services.Characteristics.notify
        ])
    }
    
    deinit {
        stopScan()
        cancel()
    }
    
    public func startScan() {
        centralManager.scanForPeripherals(withServices: services, options: nil)
    }
    
    public func stopScan() {
        centralManager.stopScan()
    }
    
    public func connect(to device: Device) {
        guard let bluetoothDevice = scannedDevices[device.id] else {
            return
        }
        let device = Device(id: device.id, name: device.name, connectionStatus: .connecting)
        deviceConnectionStatusChangedPublisher.send(device)
        centralManager.connect(bluetoothDevice.peripheral, options: nil)
    }
    
    public func disconnect(from device: Device) {
        guard let device = scannedDevices[device.id] else {
            return
        }
        centralManager.cancelPeripheralConnection(device.peripheral)
    }
    
    private func setupSubscriptions() {
        centralManager
            .publisher(for: \.isScanning)
            .sink { [weak self] (isScanning) in
                guard let self = self else {
                    return
                }
                self.isScanningPublisher.send(isScanning)
            }
            .store(in: self)
    }
}

// MARK: CBCentralManagerDelegate

extension BluetoothDeviceScanner: CBCentralManagerDelegate {
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) { }
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
                        advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        let id = peripheral.identifier.uuidString
        let device = Device(id: id, name: peripheral.name)
        let bluetoothDevice = BluetoothDevice(device: device, peripheral: peripheral)
        scannedDevices[device.id] = bluetoothDevice
        deviceScannedPublisher.send(device)
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        let id = peripheral.identifier.uuidString
        guard let bluetoothDevice = scannedDevices[id] else {
            return
        }
        
        connectedDevices[id] = bluetoothDevice
        let device = Device(id: id, name: peripheral.name, connectionStatus: .connected)
        deviceConnectionStatusChangedPublisher.send(device)
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        let id = peripheral.identifier.uuidString
        guard connectedDevices[id] != nil else {
            return
        }
        
        connectedDevices.removeValue(forKey: id)
        let device = Device(id: id, name: peripheral.name, connectionStatus: .disconnected)
        deviceConnectionStatusChangedPublisher.send(device)
    }
}

//
//  MockBluetoothPeriphery.swift
//  Networking
//
//  Created by Philip Laskowski on 27.03.21.
//

import CoreBluetoothMock

import Foundation

final public class MockBluetoothPeriphery {
    
    private init() { }
    
    public static var peripherals: [CBMPeripheralSpec] = {
        
        let blinky = CBMPeripheralSpec
            .simulatePeripheral(proximity: .outOfRange)
            .advertising(
                advertisementData: [
                    CBMAdvertisementDataLocalNameKey    : "nRF Blinky",
                    CBMAdvertisementDataServiceUUIDsKey : [CBMUUID.nordicBlinkyService],
                    CBMAdvertisementDataIsConnectable   : true as NSNumber
                ],
                withInterval: 0.250,
                alsoWhenConnected: false)
            .connectable(
                name: "nRF Blinky",
                services: [.blinkyService],
                delegate: MockCBMPeripheralSpecDelegate(),
                connectionInterval: 0.150,
                mtu: 23)
            .build()
        
        // MARK: - Physical Web Beacon (Thingy)
        let thingy = CBMPeripheralSpec
            .simulatePeripheral(proximity: .outOfRange)
            .advertising(
                advertisementData: [
                    CBMAdvertisementDataLocalNameKey    : "Thingy 52",
                    CBMAdvertisementDataServiceUUIDsKey : [CBMUUID.thingyService],
                    CBMAdvertisementDataIsConnectable   : true as NSNumber,
                    CBMAdvertisementDataServiceDataKey : [
                        CBMUUID.thingyService : Data(base64Encoded: "EO4DZ28uZ2wvcElXZGaXIA==")
                    ]
                ],
                withInterval: 0.100)
            .connectable(
                name: "Thingy 52",
                services: [.thingyService],
                delegate: MockCBMPeripheralSpecDelegate(),
                connectionInterval: 0.150,
                mtu: 23)
            .build()
        
        return [blinky, thingy]
    }()
}

public extension CBMUUID {
    static let thingyService        = CBMUUID(string: "FEAA")
    static let nordicBlinkyService  = CBMUUID(string: "00001523-1212-EFDE-1523-785FEABCD123")
    static let buttonCharacteristic = CBMUUID(string: "00001524-1212-EFDE-1523-785FEABCD123")
    static let ledCharacteristic    = CBMUUID(string: "00001525-1212-EFDE-1523-785FEABCD123")
}

private extension CBMServiceMock {
    
    static let blinkyService = CBMServiceMock(
        type: .nordicBlinkyService,
        primary: true,
        characteristics: .buttonCharacteristic, .ledCharacteristic
    )
    
    static let thingyService = CBMServiceMock(
        type: .thingyService,
        primary: true,
        characteristics: .buttonCharacteristic
    )
}

private extension CBMCharacteristicMock {
    
    static let buttonCharacteristic = CBMCharacteristicMock(
        type: .buttonCharacteristic,
        properties: [.notify, .read],
        descriptors: CBMClientCharacteristicConfigurationDescriptorMock()
    )
    
    static let ledCharacteristic = CBMCharacteristicMock(
        type: .ledCharacteristic,
        properties: [.write, .read]
    )
}

private class MockCBMPeripheralSpecDelegate: CBMPeripheralSpecDelegate {
    
    private var ledEnabled: Bool = false
    private var buttonPressed: Bool = false
    
    private var ledData: Data {
        return ledEnabled ? Data([0x01]) : Data([0x00])
    }
    
    private var buttonData: Data {
        return buttonPressed ? Data([0x01]) : Data([0x00])
    }
    
    func reset() {
        ledEnabled = false
        buttonPressed = false
    }
    
    func peripheral(_ peripheral: CBMPeripheralSpec,
                    didReceiveReadRequestFor characteristic: CBMCharacteristic) -> Result<Data, Error> {
        (characteristic.uuid == .ledCharacteristic)
            ? .success(ledData)
            : .success(buttonData)
    }
    
    func peripheral(_ peripheral: CBMPeripheralSpec,
                    didReceiveWriteRequestFor characteristic: CBMCharacteristic,
                    data: Data) -> Result<Void, Error> {
        if data.count > 0 {
            ledEnabled = data[0] != 0x00
        }
        return .success(())
    }
}

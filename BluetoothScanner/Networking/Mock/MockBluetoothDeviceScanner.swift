//
//  MockBluetoothDeviceScanner.swift
//  Networking
//
//  Created by Philip Laskowski on 27.03.21.
//

import Model

import CoreBluetoothMock

import Combine
import Foundation

private struct BluetoothDevice {
    let device: Device
    let peripheral: CBMPeripheral
}

final public class MockBluetoothDeviceScanner: DeviceScanner {
    
    public let isScanningPublisher = PassthroughSubject<Bool, Never>()
    public let deviceScannedPublisher = PassthroughSubject<Device, Never>()
    public let deviceConnectionStatusChangedPublisher = PassthroughSubject<Device, Never>()
    
    private struct Constants {
        static let queueLabel = "com.philiplaskowski.queue.mock-bluetooth-scanning"
    }
    
    private let peripherals: [CBMPeripheralSpec]
    
    private let services: [CBMUUID]
    private let queue: DispatchQueue
    private let centralManager: CBMCentralManager
    
    private var scannedDevices: [Device.ID: BluetoothDevice] = [:]
    private var connectedDevices: [Device.ID: BluetoothDevice] = [:]
    
    public init(peripherals: [CBMPeripheralSpec] = MockBluetoothPeriphery.peripherals) {
        
        if #available(iOS 13.0, *) {
            CBMCentralManagerFactory.simulateFeaturesSupport = { features in
                return features.isSubset(of: .extendedScanAndConnect)
            }
        }
        CBMCentralManagerMock.simulateInitialState(.poweredOn)
        CBMCentralManagerMock.simulatePeripherals(peripherals)
        
        self.peripherals = peripherals
        
        self.services = [CBMUUID.nordicBlinkyService, CBMUUID.thingyService]
        self.queue = DispatchQueue(label: MockBluetoothDeviceScanner.Constants.queueLabel,
                                   qos: .background,
                                   attributes: .concurrent,
                                   autoreleaseFrequency: .workItem,
                                   target: nil)
        
        self.centralManager = CBMCentralManagerFactory.instance(delegate: nil, queue: queue)
        self.centralManager.delegate = self
    }
    
    deinit {
        stopScan()
        CBMCentralManagerMock.tearDownSimulation()
    }
    
    public func startScan() {
        centralManager.scanForPeripherals(withServices: services, options: nil)
        peripherals.forEach { $0.simulateProximityChange(.immediate) }
        isScanningPublisher.send(true)
    }
    
    public func stopScan() {
        centralManager.stopScan()
        isScanningPublisher.send(false)
    }
    
    public func connect(to device: Device) {
        guard let bluetoothDevice = scannedDevices[device.id] else {
            return
        }
        let device = Device(id: device.id, name: device.name, connectionStatus: .connecting)
        deviceConnectionStatusChangedPublisher.send(device)
        centralManager.connect(bluetoothDevice.peripheral, options: nil)
    }
    
    public func disconnect(from device: Device) {
        guard let device = scannedDevices[device.id] else {
            return
        }
        centralManager.cancelPeripheralConnection(device.peripheral)
    }
}

// MARK: CBMCentralManagerDelegate

extension MockBluetoothDeviceScanner: CBMCentralManagerDelegate {
    
    public func centralManagerDidUpdateState(_ central: CBMCentralManager) { }
    
    public func centralManager(_ central: CBMCentralManager, didDiscover peripheral: CBMPeripheral,
                        advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        let id = peripheral.identifier.uuidString
        let device = Device(id: id, name: peripheral.name)
        let bluetoothDevice = BluetoothDevice(device: device, peripheral: peripheral)
        scannedDevices[device.id] = bluetoothDevice
        deviceScannedPublisher.send(device)
    }
    
    public func centralManager(_ central: CBMCentralManager, didConnect peripheral: CBMPeripheral) {
        
        let id = peripheral.identifier.uuidString
        guard let bluetoothDevice = scannedDevices[id] else {
            return
        }
        
        connectedDevices[id] = bluetoothDevice
        let device = Device(id: id, name: peripheral.name, connectionStatus: .connected)
        deviceConnectionStatusChangedPublisher.send(device)
    }
    
    public func centralManager(_ central: CBMCentralManager, didDisconnectPeripheral peripheral: CBMPeripheral, error: Error?) {
        
        let id = peripheral.identifier.uuidString
        guard connectedDevices[id] != nil else {
            return
        }
        
        connectedDevices.removeValue(forKey: id)
        let device = Device(id: id, name: peripheral.name, connectionStatus: .disconnected)
        deviceConnectionStatusChangedPublisher.send(device)
    }
}

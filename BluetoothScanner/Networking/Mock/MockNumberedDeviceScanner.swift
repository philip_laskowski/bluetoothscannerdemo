//
//  MockNumberedDeviceScanner.swift
//  BluetoothScanner
//
//  Created by Philip Laskowski on 26.03.21.
//

import Model

import Foundation
import Combine

final public class MockNumberedDeviceScanner: DeviceScanner {
    
    public let isScanningPublisher = PassthroughSubject<Bool, Never>()
    public let deviceScannedPublisher = PassthroughSubject<Device, Never>()
    public let deviceConnectionStatusChangedPublisher = PassthroughSubject<Device, Never>()
    
    private let maxNumberOfDevices: UInt
    private let deviceGenerationTimeInterval: TimeInterval
    
    private var scannedDevices: [Device.ID : Device] = [:]
    private var connectedDevices: [Device.ID : Device] = [:]
    
    private var cancellables = Set<AnyCancellable>()
    
    public init(maxNumberOfDevices: UInt = 25, deviceGenerationTimeInterval: TimeInterval = 0.25) {
        self.maxNumberOfDevices = maxNumberOfDevices
        self.deviceGenerationTimeInterval = deviceGenerationTimeInterval
    }
    
    deinit {
        stopScan()
    }
    
    public func startScan() {
        setupTimer()
        isScanningPublisher.send(true)
    }
    
    public func stopScan() {
        cancellables.forEach { $0.cancel() }
        cancellables.removeAll()
        isScanningPublisher.send(false)
    }
    
    public func connect(to device: Device) {
        transitionDevice(device, from: .connecting, to: .connected)
    }
    
    public func disconnect(from device: Device) {
        transitionDevice(device, from: .disconnecting, to: .disconnected)
    }
    
    private func transitionDevice(_ device: Device, from: Device.ConnectionStatus, to: Device.ConnectionStatus) {
        let device = Device(id: device.id, name: device.name, connectionStatus: from)
        deviceConnectionStatusChangedPublisher.send(device)
        DispatchQueue.global().asyncAfter(deadline: .now() + 1.5) { [weak self] in
            guard let self = self else {
                return
            }
            let device = Device(id: device.id, name: device.name, connectionStatus: to)
            self.connectedDevices.removeValue(forKey: device.id)
            self.deviceConnectionStatusChangedPublisher.send(device)
        }
    }
    
    private func setupTimer() {
        Timer
            .publish(every: deviceGenerationTimeInterval, on: .main, in: .common)
            .autoconnect()
            .receive(on: RunLoop.main)
            .sink(receiveValue: { [weak self] _ in
                self?.addMockDevice()
            })
            .store(in: &cancellables)
    }
    
    private func addMockDevice() {
        guard scannedDevices.count < maxNumberOfDevices else {
            cancellables.forEach { $0.cancel() }
            cancellables.removeAll()
            return
        }
        let device = Device(id: UUID().uuidString, name: "Mock Device #\(scannedDevices.count + 1)")
        scannedDevices[device.id] = device
        deviceScannedPublisher.send(device)
    }
}

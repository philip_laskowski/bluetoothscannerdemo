//
//  DeviceScanner.swift
//  BluetoothScanner
//
//  Created by Philip Laskowski on 25.03.21.
//

import Model

import Combine

public protocol DeviceScanner {
    
    var isScanningPublisher: PassthroughSubject<Bool, Never> { get }
    var deviceScannedPublisher: PassthroughSubject<Device, Never> { get }
    var deviceConnectionStatusChangedPublisher: PassthroughSubject<Device, Never> { get }
    
    func startScan()
    func stopScan()
    
    func connect(to device: Device)
    func disconnect(from device: Device)
}

//
//  ViewFactory.swift
//  View
//
//  Created by Philip Laskowski on 28.03.21.
//

import Common
import ViewModel

final public class ViewFactory<View> where View: ViewModelBased,
                                           View.ViewModel: DependenciesBased {
    
    static public func make(with dependencies: View.ViewModel.Dependencies) -> View {
        let viewModel = View.ViewModel(dependencies: dependencies)
        return View(viewModel: viewModel)
    }
    
    private init() { }
}

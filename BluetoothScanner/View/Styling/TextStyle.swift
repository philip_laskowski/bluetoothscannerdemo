//
//  TextStyle.swift
//  View
//
//  Created by Philip Laskowski on 27.03.21.
//

import SwiftUI

struct TextStyle {
    struct Border {
        let color: Color
        let cornerRadius: CGFloat
        let lineWidth: CGFloat
    }
    let foregroundColor: Color
    let font: Font
    let horizontalPadding: CGFloat
    let verticalPadding: CGFloat
    let backgroundColor: Color
    let border: Border
}

extension Text {
    func textStyle(_ style: TextStyle) -> some View {
        self.foregroundColor(style.foregroundColor)
            .font(style.font)
            .padding(.horizontal, style.horizontalPadding)
            .padding(.vertical, style.verticalPadding)
            .background(style.backgroundColor)
            .overlay(
                RoundedRectangle(cornerRadius: style.border.cornerRadius)
                    .stroke(
                        style.border.color, lineWidth: style.border.lineWidth
                    )
            )
    }
}

//
//  Styling.swift
//  View
//
//  Created by Philip Laskowski on 27.03.21.
//

import SwiftUI

struct Styling {
    
    struct Button {
        
        struct StartScanButtonStyle: ButtonStyle {
            func makeBody(configuration: Configuration) -> some View {
                configuration.label
                    .foregroundColor(.green)
                    .font(Font.body.bold())
                    .padding(10)
                    .padding(.horizontal, 20)
                    .background(Color.clear)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(
                                Color.green, lineWidth: 1
                            )
                    )
            }
        }
        
        struct StopScanButtonStyle: ButtonStyle {
            func makeBody(configuration: Configuration) -> some View {
                configuration.label
                    .foregroundColor(.red)
                    .font(Font.body.bold())
                    .padding(10)
                    .padding(.horizontal, 20)
                    .background(Color.clear)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(
                                Color.red, lineWidth: 1
                            )
                    )
            }
        }
        
        struct ConnectedButtonStyle: ButtonStyle {
            func makeBody(configuration: Configuration) -> some View {
                configuration.label
                    .foregroundColor(.red)
                    .font(Font.body.bold())
                    .padding(10)
                    .padding(.horizontal, 20)
                    .background(Color.clear)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(
                                Color.red, lineWidth: 1
                            )
                    )
            }
        }
        
        struct DisconnectedButtonStyle: ButtonStyle {
            func makeBody(configuration: Configuration) -> some View {
                configuration.label
                    .foregroundColor(.green)
                    .font(Font.body.bold())
                    .padding(10)
                    .padding(.horizontal, 20)
                    .background(Color.clear)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(
                                Color.green, lineWidth: 1
                            )
                    )
            }
        }
    }
    
    struct Text {
        
        static func DeviceTextStyle() -> TextStyle {
            TextStyle(
                foregroundColor: .black,
                font: Font.body.bold(),
                horizontalPadding: 20,
                verticalPadding: 10,
                backgroundColor: .clear,
                border: TextStyle.Border(
                    color: .clear,
                    cornerRadius: 0,
                    lineWidth: 0)
            )
        }
        
        static func ConnectingTextStyle() -> TextStyle {
            TextStyle(
                foregroundColor: .orange,
                font: Font.body.bold(),
                horizontalPadding: 20,
                verticalPadding: 10,
                backgroundColor: .clear,
                border: TextStyle.Border(
                    color: .orange,
                    cornerRadius: 10,
                    lineWidth: 1)
            )
        }
        
        static func DisconnectingTextStyle() -> TextStyle {
            TextStyle(
                foregroundColor: .orange,
                font: Font.body.bold(),
                horizontalPadding: 20,
                verticalPadding: 10,
                backgroundColor: .clear,
                border: TextStyle.Border(
                    color: .orange,
                    cornerRadius: 10,
                    lineWidth: 1)
            )
        }
    }
}

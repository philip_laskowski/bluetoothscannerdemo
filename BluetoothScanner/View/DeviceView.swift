//
//  DeviceView.swift
//  View
//
//  Created by Philip Laskowski on 28.03.21.
//

import ViewModel

import SwiftUI

struct DeviceView: View, ViewModelBased {
    
    public typealias ViewModel = DeviceViewModel
    
    public var viewModel: ViewModel
    
    public init(viewModel: ViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        Text(viewModel.dependencies.name)
            .textStyle(Styling.Text.DeviceTextStyle())
    }
}

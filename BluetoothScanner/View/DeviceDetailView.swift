//
//  DeviceDetailView.swift
//  View
//
//  Created by Philip Laskowski on 28.03.21.
//

import ViewModel

import SwiftUI

struct DeviceDetailView: View, ViewModelBased {
    
    public typealias ViewModel = DeviceDetailViewModel
    
    @ObservedObject public var viewModel: ViewModel
    
    public init(viewModel: ViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        
        let deviceView = ViewFactory<DeviceView>.make(
            with: DeviceView.ViewModel.Dependencies(
                id: viewModel.dependencies.id,
                name: viewModel.dependencies.name,
                connectionStatus: viewModel.connectionStatus
            )
        )
        
        let deviceConnectionButton = ViewFactory<DeviceConnectionButton>.make(
            with: DeviceConnectionButton.ViewModel.Dependencies(
                id: viewModel.dependencies.id,
                name: viewModel.dependencies.name,
                connectionStatus: viewModel.connectionStatus,
                deviceScanner: viewModel.dependencies.deviceScanner
            )
        )
        
        VStack {
            Spacer()
            
            deviceView
            
            deviceConnectionButton
            
            Spacer()
        }
        .navigationTitle(viewModel.dependencies.name)
    }
}

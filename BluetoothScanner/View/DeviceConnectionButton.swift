//
//  DeviceConnectionButton.swift
//  View
//
//  Created by Philip Laskowski on 28.03.21.
//

import ViewModel

import SwiftUI

struct DeviceConnectionButton: View, ViewModelBased {
    
    public typealias ViewModel = DeviceConnectionButtonViewModel
    
    @ObservedObject public var viewModel: ViewModel
    
    public init(viewModel: ViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        switch viewModel.dependencies.connectionStatus {
        case .connected:
            connectedButton
        case .connecting:
            connectingButton
        case .disconnected:
            disconnectedButton
        case .disconnecting:
            disconnectingButton
        }
    }
    
    private var connectedButton: some View {
        Button(
            action: {
                viewModel.disconnect()
            },
            label: {
                Text("Disconnect")
            }
        )
        .buttonStyle(Styling.Button.ConnectedButtonStyle())
    }
    
    private var connectingButton: some View {
        Text("Connecting...")
            .textStyle(Styling.Text.ConnectingTextStyle())
    }
    
    private var disconnectedButton: some View {
        Button(
            action: {
                viewModel.connect()
            },
            label: {
                Text("Connect")
            }
        )
        .buttonStyle(Styling.Button.DisconnectedButtonStyle())
    }
    
    private var disconnectingButton: some View {
        Text("Disconnecting...")
            .textStyle(Styling.Text.DisconnectingTextStyle())
    }
}

//
//  DeviceScannerView.swift
//  BluetoothScanner
//
//  Created by Philip Laskowski on 25.03.21.
//

import ViewModel

import SwiftUI

public struct DeviceScannerView: View, ViewModelBased {
    
    public typealias ViewModel = DeviceScannerViewModel
    
    @ObservedObject public var viewModel: ViewModel
    
    public init(viewModel: ViewModel) {
        self.viewModel = viewModel
    }
    
    public var body: some View {
        VStack {
            Spacer()
            
            header
            
            Divider()
            
            deviceList
        }
    }
    
    private var header: some View {
        HStack {
            viewModel.isScanning
                ? AnyView(
                    Button(
                        action: {
                            viewModel.stopScan()
                        },
                        label: {
                            Text("Stop Scan")
                        }
                    )
                    .buttonStyle(Styling.Button.StopScanButtonStyle())
                    .padding(.leading, 20)
                )
                : AnyView(
                    Button(
                        action: {
                            viewModel.startScan()
                        },
                        label: {
                            Text("Start Scan")
                        }
                    )
                    .buttonStyle(Styling.Button.StartScanButtonStyle())
                    .padding(.leading, 20)
                )
            
            Spacer()
        }
    }
    
    private var deviceList: some View {
        List(viewModel.devices) { deviceViewModel in
            
            let deviceDetailView = ViewFactory<DeviceDetailView>.make(
                with: DeviceDetailViewModel.Dependencies(
                    id: deviceViewModel.dependencies.id,
                    name: deviceViewModel.dependencies.name,
                    connectionStatus: deviceViewModel.dependencies.connectionStatus,
                    deviceScanner: viewModel.dependencies.deviceScanner
                )
            )
            
            let deviceView = ViewFactory<DeviceView>.make(
                with: DeviceView.ViewModel.Dependencies(
                    id: deviceViewModel.dependencies.id,
                    name: deviceViewModel.dependencies.name,
                    connectionStatus: deviceViewModel.dependencies.connectionStatus
                )
            )
            
            let deviceConnectionButton = ViewFactory<DeviceConnectionButton>.make(
                with: DeviceConnectionButton.ViewModel.Dependencies(
                    id: deviceViewModel.dependencies.id,
                    name: deviceViewModel.dependencies.name,
                    connectionStatus: deviceViewModel.dependencies.connectionStatus,
                    deviceScanner: viewModel.dependencies.deviceScanner
                )
            )
            
            NavigationLink(destination: deviceDetailView) {
                HStack {
                    Spacer()
                    
                    deviceView
                    
                    Spacer()
                    
                    deviceConnectionButton
                    
                    Spacer()
                }
            }
        }
    }
}

//
//  HasCancellable.swift
//  Networking
//
//  Created by Philip Laskowski on 27.03.21.
//

import Combine
import Foundation

final public class CancellableBag {
    fileprivate var cancellables: [Cancellable]
    public init(cancellables: [Cancellable] = []) {
        self.cancellables = cancellables
    }
}

public protocol HasCancellable: class {
    var cancellable: CancellableBag { get }
}

public extension HasCancellable {
    
    func add(_ cancellable: Cancellable) {
        DispatchQueue.global().sync { [weak self] in
            guard let self = self else {
                return
            }
            self.cancellable.cancellables.append(cancellable)
        }
    }
    
    func unsubscribe() {
        cancel()
    }
    
    func cancel() {
        DispatchQueue.global().sync {
            
            let cancellables = cancellable.cancellables
            cancellable.cancellables.removeAll()
            
            for cancellable in cancellables {
                cancellable.cancel()
            }
        }
    }
}

public extension Cancellable {
    func store(in hasCancellable: HasCancellable) {
        hasCancellable.add(self)
    }
}

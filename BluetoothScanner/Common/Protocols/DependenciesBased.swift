//
//  DependenciesBased.swift
//  ViewModel
//
//  Created by Philip Laskowski on 27.03.21.
//

public protocol DependenciesBased {
    associatedtype Dependencies
    var dependencies: Dependencies { get }
    init(dependencies: Dependencies)
}
